﻿using MailKit.Net.Pop3;

using Mz.Bot.Discord.Trash.Models;

namespace Mz.Bot.Discord.Trash.Managers
{
    internal class MailManager
    {
        public Pop3Client SetupMailClient(string mailEndpoint,
            BasicCredential mailCredential, int mailPort = 995,
            bool useSsl = true)
        {
            var mailClient = new Pop3Client();
            mailClient.Connect(mailEndpoint, mailPort, useSsl);
            mailClient.Authenticate(mailCredential.Username,
                mailCredential.Password);
            return mailClient;
        }
    }
}
