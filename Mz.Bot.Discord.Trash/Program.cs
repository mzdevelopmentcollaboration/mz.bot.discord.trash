using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Mz.Bot.Discord.Trash
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices(services =>
                {
                    services.AddHostedService<MailWorker>();
                });
    }
}
