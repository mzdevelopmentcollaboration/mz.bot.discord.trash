using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using MailKit.Net.Pop3;
using Microsoft.Extensions.Configuration;
using MimeKit;
using Mz.Bot.Discord.Trash.Managers;
using Mz.Bot.Discord.Trash.Models;

namespace Mz.Bot.Discord.Trash
{
    public class MailWorker : BackgroundService
    {
        private readonly Pop3Client _mailClient;
        private readonly ILogger<MailWorker> _logger;

        public MailWorker(ILogger<MailWorker> logger,
            IConfiguration configuration)
        {
            _logger = logger;
            try
            {
                _mailClient = new MailManager().SetupMailClient(
                    configuration["MailSettings:Server"],
                    new BasicCredential
                    {
                        Username = Environment
                            .GetEnvironmentVariable("mailUsername"),
                        Password = Environment
                            .GetEnvironmentVariable("mailPassword")
                    });
            }
            catch (Exception genericException)
            {
                _logger.LogCritical(genericException,
                    genericException.Message);
                throw;
            }
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                var messageCount = await _mailClient
                    .GetMessageCountAsync(cancellationToken);
                var messageCollection = new List<MimeMessage>();
                if (messageCount < 5)
                {
                    for (var i = 0; i < messageCount; i++)
                    {
                        messageCollection.Add(
                            await _mailClient.GetMessageAsync(i, cancellationToken));
                    }
                }
                else
                {
                    for (var i = messageCount - 5; i < messageCount; i++)
                    {
                        messageCollection.Add(
                            await _mailClient.GetMessageAsync(i, cancellationToken));
                    }
                }
                messageCollection = messageCollection.Where(x =>
                    x.From.Mailboxes.First().Address ==
                        "erinnerungsservice@abfallkalender.awb-mainz-bingen.de")
                    .ToList();
                await Task.Delay(3600000, cancellationToken);
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _mailClient.DisconnectAsync(true, cancellationToken);
            _mailClient.Dispose();
            return base.StopAsync(cancellationToken);
        }
    }
}
